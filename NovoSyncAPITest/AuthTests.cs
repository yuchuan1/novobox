﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Delta.NovoSync.Client.API;
using Delta.NovoSync.Client.Model;
using NUnit.Framework;
using StructureMap;

namespace Delta.NovoSyncClient.Tests
{
    [TestFixture]
    public class AuthTests
    {
        [TestFixtureSetUp]
        public void Init()
        {
        }

        [Test]
        public void ResetAuthClearsAuthTable()
        {
            try
            {
                Auth.Reset();
                
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
            
             using(var db = new NovoSyncDBEntities())
             {
                 Assert.AreEqual(0, db.Auths.Count());
             }
        }
    }
}
