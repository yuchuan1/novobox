﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Delta.NovoSync.Client;
using Delta.NovoSync.Client.API;
using Delta.NovoSync.Client.Model;
using NUnit.Framework;
using Newtonsoft.Json;
using StructureMap;

namespace Delta.NovoSyncClient.Tests
{
    [TestFixture]
    public class MetaDataTests
    {
       [TestFixtureSetUp]
        public void Init()
        {
           
        }

        [TestFixtureTearDown]
        public void Dispose()
        {
            
        }
        
        [Test]
        public void TestMetadataEqualityForPath()
        {
            string metadataA = @"[
            {
            'path': '/test.txt',
            'type': 'FILE',
            'rev':  1
            },
            {
            'path': '/sub/test1.txt',
            'type': 'FILE',
            'rev':  2
            }]";

            string metadataB = @"[
            {
            'path': '/test.txt',
            'type': 'FILE',
            'rev':  1
            },
            {
            'path': '/sub/test1.txt',
            'type': 'FILE',
            'rev':  2
            }]";

            var listA = MetaData.Parse(metadataA);
            var listB = MetaData.Parse(metadataB);
            

            bool result = listA.Equals(listB);

            Assert.AreEqual(false,result);
        }

        [Test]
        public void ParseMultipleMetaData()
        {
            string metadata = @"[
            {
            'path': '/test.txt',
            'type': 'FILE',
            'revision':  '1'
            },
            {
            'path': '/sub/test1.txt',
            'type': 'FILE',
            'revision':  '2'
            }]";

            var result = MetaData.Parse(metadata);
            
            Assert.AreEqual(result.Count, 2);

            Assert.AreEqual( @"/test.txt",result[0].Path);
            Assert.AreEqual( @"FILE",result[0].Type);
            Assert.AreEqual( "1",result[0].Revision);

            Assert.AreEqual( @"/sub/test1.txt",result[1].Path);
            Assert.AreEqual( @"FILE",result[1].Type);
            Assert.AreEqual( "2",result[1].Revision);

        }

        [Test]
        public void ParseSingleMetaData()
        {
            string metadata = @"[
            {
            'path': '/test.txt',
            'type': 'FILE',
            'revision':  '1'
            }]";

            var result = MetaData.Parse(metadata);

            Assert.AreEqual(result.Count, 1);

            Assert.AreEqual(@"/test.txt",result[0].Path);
            Assert.AreEqual( @"FILE",result[0].Type);
            Assert.AreEqual("1", result[0].Revision);

        }

        [Test]
        public void ParseEmptyMetaData()
        {
            string metadata = @"[]";

            var result = MetaData.Parse(metadata);

            Assert.AreEqual(0, result.Count);
        }

        [Test]
        [ExpectedException(typeof(JsonSerializationException))]
        public void ParseFaultyMetaDataReturnsSerializationException()
        {
            string metadata = @"'path': '/test.txt',
            'type': 'FILE',
            'revision':  '1'";

            var result = MetaData.Parse(metadata);
            
        }

        [Test]
        [ExpectedException(typeof(System.ArgumentNullException))]
        public void ParseNullMetaDataReturnsArgumentIsNullException()
        {
            string metadata = null;
            var result = MetaData.Parse(metadata);

        }

       [Test]
       public void TestGetFileName()
       {
           MetaData metaData = new MetaData();
           metaData.Path = @"/test/test1/test2/testFile.txt";
           metaData.Type = "FILE";

           Assert.AreEqual("testFile.txt", metaData.GetFileName());
       }

       [Test]
       public void TestGetEmptyFileName()
       {
           MetaData metaData = new MetaData();
           metaData.Path = @"/test/test1/test2/";
           metaData.Type = "DIRECTORY";

           Assert.AreEqual("", metaData.GetFileName());
       }

        [Test]
        public void TestGetFolderPath()
        {
            MetaData metaData = new MetaData();
            metaData.Path = @"/test/1/2/testFile.txt";
            metaData.Type = "FILE";

            Assert.AreEqual("/test/1/2/", metaData.GetFolderPath());
        }
    }
}
