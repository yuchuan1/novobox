﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Delta.NovoSync.Client.Model;
using NUnit.Framework;

namespace Delta.NovoSyncClient.Tests
{
    [TestFixture]
    public class SQLiteDBTests
    {
        NovoSyncDBEntities db;
        [TestFixtureSetUp]
        public void Init()
        {
            db = new NovoSyncDBEntities();
            foreach (var auth in db.Auths)
            {
                db.Auths.Remove(auth);
            }

            foreach (var metadata in db.MetaDatas)
            {
                db.MetaDatas.Remove(metadata);
            }

            db.SaveChanges();
        }

        [TestFixtureTearDown]
        public void Dispose()
        {
            foreach (var auth in db.Auths)
            {
                db.Auths.Remove(auth);
            }

            foreach (var metadata in db.MetaDatas)
            {
                db.MetaDatas.Remove(metadata);
            }

            db.SaveChanges();
            db.Dispose();
        }

        [Test]
        public void TestSaveAuth()
        {
            Auth auth = new Auth();
            auth.AccessToken = "TestAccess1";
            auth.RequestToken = "TestRequest";
            auth.Password = @"""'a1qaz~!@#$%^&*()_+`";
            auth.UserEmail = "test@test.com";

            Auth dbAuth;
            try
            {
                db.Auths.Add(auth);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

            dbAuth = db.Auths.Find("TestRequest");

            Assert.AreEqual("TestAccess1",dbAuth.AccessToken);
            Assert.AreEqual("TestRequest",dbAuth.RequestToken);
            Assert.AreEqual( "test@test.com",dbAuth.UserEmail);
            Assert.AreEqual( @"""'a1qaz~!@#$%^&*()_+`",dbAuth.Password);

            db.Auths.Remove(dbAuth);
            db.SaveChanges();
        }

        [Test]
        public void TestSaveMetaData()
        {
            MetaData metaData = new MetaData();
            metaData.Path = @"/folder/subfolder/file.txt";
            metaData.Revision = "re342rewfac w432q3";
            metaData.Type = "FILE";

            try
            {
                db.MetaDatas.Add(metaData);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

            var dbMetaData = db.MetaDatas.Find(@"/folder/subfolder/file.txt");

            Assert.AreEqual(metaData.Path,dbMetaData.Path);
            Assert.AreEqual( metaData.Revision,dbMetaData.Revision);
            Assert.AreEqual( metaData.Type,dbMetaData.Type);

        }
    }
}
