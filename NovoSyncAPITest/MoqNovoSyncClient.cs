﻿using System;
using System.Collections.Generic;
using System.IO;
using Delta.NovoSync.Client;
using Delta.NovoSync.Client.API;
using Delta.NovoSync.Client.Model;
using AccountInfo = Delta.NovoSync.Client.API.AccountInfo;

namespace Delta.NovoSyncClient.Tests
{
    public class MoqNovoSyncClient : INovoSyncClient
    {
        private string _accessToken = "test_access_token";
        private string _requestToken = "test_request_token";
        public string RootFolderName = @"C:\Users\Eddie\NovoBox";

        public MoqNovoSyncClient(string deviceName, string rootFolderPath)
        {
            Auth.Reset();

            Auth auth = new Auth();
            auth.AccessToken = _accessToken;
            auth.RequestToken = _requestToken;
            
            using(var db = new NovoSyncDBEntities())
            {
                db.Auths.Add(auth);
            }
        }

        #region INovoSyncClient Members

        public void Initialize(string deviceName, string rootFolderPath, bool autoOpenBrowser)
        {
            throw new NotImplementedException();
        }

        public string GetRequestToken()
        {
            return _requestToken;
        }

        public string Authorize( string deviceName)
        {
            string BaseUrl = "http://www.google.com";
            return BaseUrl;
        }

        public string GetAccessToken()
        {
            return _accessToken;
        }

        public string GetAccessToken(string requestToken)
        {
            throw new NotImplementedException();
        }


        public bool GetIsAuthorized()
        {
            throw new NotImplementedException();
        }

        public void Init(string deviceName, string rootFolderPath)
        {
            throw new NotImplementedException();
        }

        public void ResetAuth()
        {
            throw new NotImplementedException();
        }

        public void Sync()
        {
            throw new NotImplementedException();
        }

        public string RenameFile(string path, string newFileName)
        {
            throw new NotImplementedException();
        }

        public string DeleteFile(string path)
        {
            throw new NotImplementedException();
        }

        public string UploadFile(string path)
        {
            throw new NotImplementedException();
        }

        public string DownloadFile(string path)
        {
            throw new NotImplementedException();
        }

        public IList<MetaData> GetMetaData()
        {
            throw new NotImplementedException();
        }

        public void CreateFolder(string path)
        {
            throw new NotImplementedException();
        }

        public void Rename(string path, string newFileNameFullPath)
        {
            throw new NotImplementedException();
        }

        public void CreateFolders(MetaData metadata)
        {
            string synPath = RootFolderName + metadata.GetFolderPath();
            var di = new DirectoryInfo(synPath);
            di.CreateDirectory();
        }

        public string GetMetaData(string path)
        {
            throw new NotImplementedException();
        }

        public string GetRootPath()
        {
            throw new NotImplementedException();
        }

        public Auth GetAuth()
        {
            throw new NotImplementedException();
        }

        public List<MetaData> FakeGetMetaData(string metadataJson)
        {
            throw new NotImplementedException();
        }

        #endregion

        public AccountInfo GetAccountInfo()
        {
            throw new NotImplementedException();
        }

        public string DownloadMetadata()
        {
            throw new NotImplementedException();
        }

        public string DownloadMetadata(string file, string path)
        {
            throw new NotImplementedException();
        }

        public string DownloadFile(string file, string path)
        {
            throw new NotImplementedException();
        }

        public string UploadFile(string file, string path)
        {
            throw new NotImplementedException();
        }

        public string DeleteFile(string file, string path)
        {
            throw new NotImplementedException();
        }

        public string RenameFile(string file, string path, string newFileName)
        {
            throw new NotImplementedException();
        }

        public string MoveFile(string path, string file, string newPath)
        {
            throw new NotImplementedException();
        }


    }
}