﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Delta.NovoSync.Client.API;
using Delta.NovoSync.Client.Model;
using NUnit.Framework;
using StructureMap;

namespace NovoSyncClientIntegrationTests
{
    [TestFixture]
    public class FileOperationTests
    {
        private NovoSyncClient _concreteClient;
        NovoSyncDBEntities db;
        private string _rootFolderPath;

        [TestFixtureSetUp]
        public void Init()
        {
            // clear database content
            db = new NovoSyncDBEntities();
            foreach (var auth in db.Auths)
            {
                db.Auths.Remove(auth);
            }
            db.SaveChanges();
            

            Auth testAuth = new Auth();
            testAuth.AccessToken = "test_access_token";
            testAuth.RequestToken = "test_request_token";
            testAuth.UserEmail = "";
            testAuth.Password = "";
            testAuth.Save();

            _rootFolderPath = "./test";
            string deviceName = System.Net.Dns.GetHostName();

            ObjectFactory.Initialize(x => x.For<INovoSyncClient>().Use<NovoSyncClient>()
                 .WithProperty("rootFolderPath").EqualTo(_rootFolderPath)
                 .WithProperty("deviceName").EqualTo(deviceName)
                 .WithProperty("autoOpenBrowser").EqualTo(false));

            _concreteClient = new NovoSyncClient("test", "test", false);
        }

        [TestFixtureTearDown]
        public void Dispose()
        {
            db.Dispose();
        }

        [Test]
        public void TestFileUpload()
        {
            var dir = new DirectoryInfo(_rootFolderPath);

            using (var file = new FileInfo(Path.Combine(dir.FullName, "upload.txt")).Create())
            {
            }

            var fi = new FileInfo(Path.Combine(dir.FullName, "upload.txt"));
            if (fi.Exists)
            {
                _concreteClient._accessToken = "test_access_token";
                var UploadMetadata = NovoSyncClient.UploadFile(_concreteClient, "/upload.txt");
                var metadata = _concreteClient.GetMetaData(_concreteClient, "/upload.txt");
                Assert.AreEqual(1, metadata.Count(m => m.Path.Equals("/upload.txt") && m.Revision == UploadMetadata[0].Revision));
            }
            else
            {
                Assert.Fail("Test file did not get created!");
            }

            fi.Delete();
            NovoSyncClient.DeleteFile(_concreteClient, "/upload.txt");

        }

        [Test]
        public void TestFileDelete()
        {
            var dir = new DirectoryInfo(_rootFolderPath);

            using (var file = new FileInfo(Path.Combine(dir.FullName, "delete.txt")).Create())
            {
            }
            _concreteClient._accessToken = "test_access_token";
            

                var UploadMetadata = NovoSyncClient.UploadFile(_concreteClient, "/delete.txt");

                    var fi = new FileInfo(Path.Combine(dir.FullName, "delete.txt"));
                    fi.Delete();
                    var DeleteMetadata = NovoSyncClient.DeleteFile(_concreteClient, "/delete.txt");
                    var metadata = _concreteClient.GetMetaData(_concreteClient, "/delete.txt");
                    Assert.IsNull(metadata);
              
            
        }

        [Test]
        public void TestCreateEmptyFolder()
        {
            var dir = new DirectoryInfo(_rootFolderPath);
            _concreteClient._accessToken = "test_access_token";
            var createFolderMetadata = NovoSyncClient.CreateFolder(_concreteClient, "/emptyFolder");
            var metadata = _concreteClient.GetMetaData(_concreteClient, "/emptyFolder");
            Assert.AreEqual(1,
                            metadata.Count(
                                m =>
                                m.Path.Equals("/emptyFolder") && m.Revision.Equals(createFolderMetadata[0].Revision)));

            NovoSyncClient.DeleteFile(_concreteClient, "/emptyFolder");
        }
    }
}
