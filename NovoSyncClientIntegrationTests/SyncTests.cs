﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Delta.NovoSync.Client.Model;
using NUnit.Framework;
using Delta.NovoSync.Client.API;

namespace NovoSyncClientIntegrationTests
{
    [TestFixture]
    public class SyncTests
    {
        private NovoSyncClient _client;
        private string _rootFolderPath;

        [TestFixtureSetUp]
        public void Init()
        {
            _rootFolderPath = "test";
            _client = new NovoSyncClient("test", _rootFolderPath, false);

            Auth testAuth = new Auth();
            testAuth.AccessToken = "test_access_token";
            testAuth.RequestToken = "test_request_token";
            testAuth.UserEmail = "";
            testAuth.Password = "";
            testAuth.Save();
        }

        [Test]
        public void TestSyncFolder()
        {
            var di = new DirectoryInfo(_rootFolderPath);
            var sub = di.CreateSubdirectory("TestSubFolder");

            _client.SyncPhysicalFolder();

            IList<MetaData> metadatas = _client.GetMetaData(_client);
            var path = sub.FullName.Replace(di.FullName,string.Empty).Replace(@"\", @"/");
            int count = (from item in metadatas
                         where item.Type == "DIRECTORY"
                               && item.Path == path.Replace(@"/", @"\")
                         select item).Count();

            Assert.AreEqual(1, count);
            
            sub.Delete(true);
            NovoSyncClient.DeleteFile(_client, path.Replace(@"/", @"\"));
            
            Assert.AreEqual(1, count);

        }

        [Test]
        public void TestSyncFile()
        {
            IList<MetaData> metadatas = _client.GetMetaData(_client);
            var di = new DirectoryInfo(_rootFolderPath);
            using (var file = new FileInfo(Path.Combine(di.FullName, "NewFile.txt")).Create())
            {
            }

            var fi = new FileInfo(Path.Combine(di.FullName, "NewFile.txt"));
            var path = fi.FullName.Replace(di.FullName, string.Empty).Replace(@"\", @"/");
            var count = (from item in metadatas
                     where item.Type == "FILE"
                           && item.Path == path.Replace(@"/", @"\")
                     select item).Count();

            fi.Delete();
            NovoSyncClient.DeleteFile(_client, path.Replace(@"/", @"\"));
            
        }

    }
}
