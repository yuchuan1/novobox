﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Delta.NovoSync.Client.API;
using Delta.NovoSync.Client.Model;
using NUnit.Framework;
using StructureMap;

namespace NovoSyncClientIntegrationTests
{
    [TestFixture]
    public class AuthTests
    {
        private INovoSyncClient _client;
        NovoSyncDBEntities db;

        [TestFixtureSetUp]
        public void Init()
        {
            // clear database content
            db = new NovoSyncDBEntities();
            foreach (var auth in db.Auths)
            {
                db.Auths.Remove(auth);
            }
            db.SaveChanges();

            string rootFolderPath = (Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "/" + "NovoBox").Replace("\\","/");
            string deviceName = System.Net.Dns.GetHostName();

           ObjectFactory.Initialize(x => x.For<INovoSyncClient>().Use<NovoSyncClient>()
                .WithProperty("rootFolderPath").EqualTo(rootFolderPath)
                .WithProperty("deviceName").EqualTo(deviceName)
                .WithProperty("autoOpenBrowser").EqualTo(false));

            _client = ObjectFactory.GetInstance<INovoSyncClient>();


        }


        [TestFixtureTearDown]
        public void Dispose()
        {
            foreach (var auth in db.Auths)
            {
                db.Auths.Remove(auth);
            }
            db.SaveChanges();
            db.Dispose();
        }

        [Test]
        public void TestGetRequestToken()
        {
            _client.ResetAuth();
            string requestToken = _client.GetRequestToken();

            Assert.AreNotEqual(string.Empty, requestToken);
        }

        [Test]
        public void TestGetAccessToken()
        {
            _client.ResetAuth();

            var accessToken = _client.GetAccessToken("test_request_token");
            Assert.AreNotEqual(string.Empty, accessToken);
            Assert.True(accessToken.Contains("test_access_token"));
        }
    }
}
