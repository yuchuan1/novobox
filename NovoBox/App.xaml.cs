﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;

namespace Delta.NovoBox
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public class App : Application, ISingleInstanceApp
    {
        private const string Unique = "My_Unique_Application_String";

        #region ISingleInstanceApp Members

        public bool SignalExternalCommandLineArgs(IList<string> args)
        {
            // handle command line arguments of second instance
            // ...
            return true;
        }

        #endregion

        [DebuggerNonUserCode]
        [GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent()
        {
#line 2 "..\..\App.xaml"
            StartupUri = new Uri("MainWindow.xaml", UriKind.Relative);

#line default
#line hidden
        }

        [STAThread]
        [DebuggerNonUserCode]
        [GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
        public static void Main()
        {
            if (SingleInstance<App>.InitializeAsFirstInstance(Unique))
            {
                var application = new App();
                application.InitializeComponent();
                application.Run();
                // Allow single instance code to perform cleanup operations
                SingleInstance<App>.Cleanup();
            }
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            if (e.Args.Any())
            {
                var test = e.Args[0];
            }

            base.OnStartup(e);
        }
    }
}