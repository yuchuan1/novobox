﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using Delta.NovoBox.Utils;
using Delta.NovoSync.Client;
using Delta.NovoSync.Client.API;
using Delta.NovoSync.Client.Exceptions;
using Hardcodet.Wpf.TaskbarNotification;
using IWshRuntimeLibrary;
using Microsoft.Win32;
using NLog;
using StructureMap;

namespace Delta.NovoBox
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private static readonly object Lock = new object();
        private static INovoSyncClient _client;
        private readonly Icon _defaultIcon = new Icon("images/folder.ico");
        private readonly Icon _stopSignIcon = new Icon("images/STOPSIGN.ICO");
        private readonly FileSystemWatcher _dirWatcher = new FileSystemWatcher();
        private readonly FileSystemWatcher _fileWatcher = new FileSystemWatcher();

        private readonly string _rootFolderPath;
        private readonly FolderBrowserDialog folderBrowserDialog;
        private string ProductName = "NovoBox";

        public string deviceName = System.Net.Dns.GetHostName();
        // internal TaskbarIcon tbi = new TaskbarIcon();

        bool novoSyncServerIsAvailable = false;

        public MainWindow()
        {
            Visibility = Visibility.Hidden;
            InitializeComponent();
            var sb = new StringBuilder();
            sb.Append("ID: ");
            sb.AppendLine(ConfigurationManager.AppSettings["id"]);

            folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.Description = "Select the directory that you want to use as the default.";

            if (ConfigurationManager.AppSettings["DefaultFolder"].Trim() != string.Empty)
                _rootFolderPath = ConfigurationManager.AppSettings["DefaultFolder"].Replace(@"/", @"\");
            else
            {
                _rootFolderPath =
                    (Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "/" + ProductName).Replace("\\", "/");
            }

            try
            {
                ObjectFactory.Initialize(x => x.For<INovoSyncClient>().Use<NovoSyncClient>()
                                             .WithProperty("rootFolderPath").EqualTo(_rootFolderPath)
                                             .WithProperty("deviceName").EqualTo(deviceName)
                                             .WithProperty("autoOpenBrowser").EqualTo(true));

                _client = ObjectFactory.GetInstance<INovoSyncClient>();
                novoSyncServerIsAvailable = true;
            }
            catch (Exception ex)
            {
                novoSyncServerIsAvailable = false;
                tbi.Icon = _stopSignIcon;
                tbi.ToolTipText = "NovoBox is offline";
            }

            if (novoSyncServerIsAvailable)
            {
                tbi.Icon = _defaultIcon;
                tbi.ToolTipText = ProductName;
                tbi.MenuActivation = PopupActivationMode.RightClick;
                tbi.TrayMouseDoubleClick += OnTrTrayMouseDoubleClick;

                txtNovoBoxDefaultFolder.Text = _rootFolderPath;
                txtNovoSyncServer.Text = ConfigurationManager.AppSettings["NovoSyncBaseUrl"];



                if (Util.IsAutoStartEnabled("NovoBox"))
                {
                    this.chkAutoStart.IsChecked = true;
                }
                else
                {
                    this.chkAutoStart.IsChecked = false;
                }
                Authorize();
            }
            
                

        }

        private void Authorize()
        {
            if (_client.GetIsAuthorized())
            {
                //   lblStatus.Content = "This computer is linked to NovoSync Server with " + _client.GetAccountInfo().Email + "\n Root Folder: " + _rootFolderPath;
                lblStatus.Content = "This computer is linked to NovoSync Server\n Root Folder: " + _rootFolderPath;
                btnLink.Visibility = Visibility.Hidden;
                btnUnlink.Visibility = Visibility.Visible;
                Visibility = Visibility.Hidden;
                //tbi.ShowBalloonTip("NovoBox", "Linked to NovoSync Server", BalloonIcon.Info);
                MonitorFolder();
            }
            else
            {
                lblStatus.Content =
                    "This computer is NOT linked to NovoSync Server\nPlease go to the web UI to add this device";
                btnLink.Visibility = Visibility.Visible;
                btnUnlink.Visibility = Visibility.Hidden;
                Visibility = Visibility.Visible;
            }
        }

        private void MonitorFolder()
        {
            var watchedFolder = new DirectoryInfo(_rootFolderPath);
            if (watchedFolder.Exists)
            {
                if (watchedFolder.GetFiles().Length == 1 && watchedFolder.GetDirectories().Length == 0)
                {
                    try
                    {
                        _client.Sync();
                        lblStatus.Content = "\nInitial Sync complete!";
                    }
                    catch (Exception ex)
                    {
                        _logger.Warn(ex.Message);
                        if (ex.InnerException != null)
                            _logger.Warn(ex.InnerException.Message);
                        throw;
                    }
                }

                BeginMonitoringFolder(watchedFolder);
            }
            else
            {
                try
                {
                    // Create NovoSync folder under c:\user\username  ( User profile folder )
                    watchedFolder.Create();
                    var myFolderIcon = new FolderIcon(_rootFolderPath);
                    myFolderIcon.CreateFolderIcon(Directory.GetCurrentDirectory() + @"\images\folder.ico", "NovoBox");

                    // add a shortcut of NovoSync folder to favorites
                    string favoritesFolder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) +
                                             "\\Links";
                    var shell = new WshShellClass();
                    var shortcut = (IWshShortcut) shell.CreateShortcut(favoritesFolder + "\\NovoBox.lnk");
                    // shortcut.TargetPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                    shortcut.TargetPath = _rootFolderPath;
                    shortcut.Description = "NovoBox";
                    shortcut.IconLocation = Directory.GetCurrentDirectory() + @"\images\folder.ico";
                    shortcut.Save();
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message);
                    throw;
                }

                BeginMonitoringFolder(watchedFolder);
            }
        }

        private void OnTrTrayMouseDoubleClick(object sender, RoutedEventArgs e)
        {
            Show();
            BringIntoView();
            Focus();
            WindowState = WindowState.Normal;
        }


        private void Window_SizeChanged_1(object sender, SizeChangedEventArgs e)
        {
        }

        private void Window_StateChanged_1(object sender, EventArgs e)
        {
            if (WindowState == WindowState.Minimized)
                Hide();
        }

        public void StopMonitoringFolder(DirectoryInfo watchedFolder)
        {
            _fileWatcher.EnableRaisingEvents = false;
            _dirWatcher.EnableRaisingEvents = false;
        }

        public void BeginMonitoringFolder(DirectoryInfo watchedFolder)
        {
            //* using windows file watcher
            const string strFilter = "*.*";
            _fileWatcher.Path = watchedFolder.FullName;
            _fileWatcher.Filter = strFilter;
            _fileWatcher.NotifyFilter = NotifyFilters.FileName;
            _fileWatcher.IncludeSubdirectories = true;
            _fileWatcher.Changed += OnChanged;
            _fileWatcher.Created += OnChanged;
            _fileWatcher.Deleted += OnChanged;
            //_fileWatcher.Renamed += OnChanged;
            _fileWatcher.Renamed += OnRenmaed;
            _fileWatcher.EnableRaisingEvents = true;


            _dirWatcher.Path = watchedFolder.FullName;
            _dirWatcher.Filter = strFilter;
            _dirWatcher.NotifyFilter = NotifyFilters.DirectoryName;
            _dirWatcher.IncludeSubdirectories = true;
            _dirWatcher.Changed += OnChanged;
            _dirWatcher.Created += OnChanged;
            _dirWatcher.Deleted += OnChanged;
            _dirWatcher.Renamed += OnChanged;
            _dirWatcher.EnableRaisingEvents = true;

            // */
        }

        private void OnRenmaed(object sender, RenamedEventArgs e)
        {
            var fi = new FileInfo(e.FullPath);
            if (fi.Exists)
            {
                try
                {
                    NovoSyncClient.RenameFile(_client,
                                              e.OldFullPath.Replace("\\", "/").Replace(_rootFolderPath, string.Empty),
                                              e.FullPath.Replace("\\", "/").Replace(_rootFolderPath, string.Empty));
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message);
                    throw;
                }
            }
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            string path = e.FullPath.Replace("\\", "/").Replace(_rootFolderPath, string.Empty);

            if (sender == _fileWatcher)
            {
                if (e.ChangeType == WatcherChangeTypes.Deleted)
                {
                    NovoSyncClient.DeleteFile(_client, path);
                }

                if (e.ChangeType == WatcherChangeTypes.Changed || e.ChangeType == WatcherChangeTypes.Created)
                {
                    var fi = new FileInfo(e.FullPath);
                    if (fi.Exists)
                    {
                        NovoSyncClient.UploadFile(_client, path);
                    }
                }


                if (e.ChangeType == WatcherChangeTypes.Renamed)
                {
                    var fi = new FileInfo(e.FullPath);
                    if (fi.Exists)
                    {
                        try
                        {
                            NovoSyncClient.RenameFile(_client, path, e.FullPath);
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex.Message);
                            throw;
                        }
                    }

                    try
                    {
                        NovoSyncClient.RenameFile(_client, path, e.FullPath);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex.Message);
                        throw;
                        if (e.ChangeType == WatcherChangeTypes.Deleted)
                        {
                            NovoSyncClient.DeleteFile(_client, path);
                        }
                    }
                }
            }

            if (sender == _dirWatcher)
            {
                if (e.ChangeType == WatcherChangeTypes.Created)
                {
                    var di = new DirectoryInfo(e.FullPath);
                    if (di.Exists)
                        _client.CreateFolder(e.FullPath.Replace("\\", "/").Replace(_client.GetRootPath(),
                                                                                   string.Empty));

                    foreach (string file in Directory.EnumerateFiles(e.FullPath, "*.*", SearchOption.AllDirectories)
                        )
                    {
                        var fi = new FileInfo(file);
                        if (fi.Exists)
                        {
                            {
                                NovoSyncClient.UploadFile(_client,
                                                          fi.FullName.Replace("\\", "/").Replace(
                                                              _client.GetRootPath(),
                                                              string.Empty));
                            }
                        }
                    }

                    foreach (
                        string folder in
                            Directory.EnumerateDirectories(e.FullPath, "*.*", SearchOption.AllDirectories))
                    {
                        di = new DirectoryInfo(folder);
                        if (di.GetDirectories().Length == 0 && di.GetFiles().Length == 0)
                        {
                            // create empty folder
                            _client.CreateFolder(di.FullName);
                        }
                    }
                }

                if (e.ChangeType == WatcherChangeTypes.Deleted)
                {
                    NovoSyncClient.DeleteFile(_client, path);
                }
            }
        }


        private void PreferencesClick
            (object sender, RoutedEventArgs e)
        {
            Visibility = Visibility.Visible;
        }

        private
            void QuitClick
            (object sender, RoutedEventArgs e)
        {
            _fileWatcher.Dispose();
            _dirWatcher.Dispose();
            Environment.Exit(0);
        }

        private
            void btnLink_Click
            (object sender, RoutedEventArgs e)
        {
            _client.Initialize(deviceName, _rootFolderPath, true);
            Authorize();
        }

        private
            void openNovoBoxFolder_Click
            (object sender, RoutedEventArgs e)
        {
            Process.Start(_client.GetRootPath());
        }

        private
            void launchWebSite_Click
            (object sender, RoutedEventArgs e)
        {
            string browser = BrowserUtil.getDefaultBrowser();
            string url = ConfigurationManager.AppSettings["NovoSyncBaseUrl"];

            var process = new Process
                {
                    StartInfo =
                        {
                            FileName = browser,
                            Arguments = url,
                            WindowStyle = ProcessWindowStyle.Normal
                        }
                };
            process.Start();
        }

        private
            void about_Click
            (object sender, RoutedEventArgs e)
        {
            var about = new About();
            about.Visibility = Visibility.Visible;
        }

        private
            void btnUnlink_Click
            (object sender, RoutedEventArgs e)
        {
            var watchedFolder = new DirectoryInfo(_rootFolderPath);
            StopMonitoringFolder(watchedFolder);
            _client.ResetAuth();
            btnLink.Visibility = Visibility.Visible;
            btnUnlink.Visibility = Visibility.Hidden;
        }

        private
            void btnCancel_Click
            (object sender, RoutedEventArgs e)
        {
            // call init
            Visibility = Visibility.Hidden;
        }

        private
            void Button_Click_1
            (object sender, RoutedEventArgs e)
        {
            DialogResult result = folderBrowserDialog.ShowDialog();
            txtNovoBoxDefaultFolder.Text = folderBrowserDialog.SelectedPath;
        }

        private
            void chkAutoStart_Checked
            (object sender, RoutedEventArgs e)
        {
            if (chkAutoStart.IsChecked != null && chkAutoStart.IsChecked.Value)
                Util.SetAutoStart("NovoBox", Assembly.GetExecutingAssembly().Location);
        }

        private
            void Window_Closing_1
            (object sender, CancelEventArgs e)
        {
            _fileWatcher.Dispose();
            _dirWatcher.Dispose();
        }

        private
            void Button_Click_2
            (object sender, RoutedEventArgs e)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            AppSettingsSection app = config.AppSettings;
            app.Settings.Remove("DefaultFolder");
            app.Settings.Add("DefaultFolder", txtNovoBoxDefaultFolder.Text.Replace(@"\", "/").Trim());

            app.Settings.Remove("NovoSyncBaseUrl");
            app.Settings.Add("NovoSyncBaseUrl", txtNovoSyncServer.Text.Trim());
            config.Save(ConfigurationSaveMode.Modified);
        }

        private void chkAutoStart_Unchecked(object sender, RoutedEventArgs e)
        {
            Util.UnSetAutoStart("NovoBox");
        }
    }
}