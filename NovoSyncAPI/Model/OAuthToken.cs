﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Delta.NovoSync.Client.Model
{
    public class OAuthToken
    {
        public string oauth_token { get; set; }
        public static Model.OAuthToken Parse(string oauthTokenJson)
        {
            var token = JsonConvert.DeserializeObject<Model.OAuthToken>(oauthTokenJson);
            return token;
        }
    }
}
