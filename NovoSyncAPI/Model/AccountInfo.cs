﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Delta.NovoSync.Client.Model
{
    public class AccountInfo
    {
        public string Email { get; set; }
        public long? Quota { get; set; }
    }
}
