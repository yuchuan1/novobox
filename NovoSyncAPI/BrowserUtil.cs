﻿using Microsoft.Win32;

namespace Delta.NovoSync.Client
{
    public class BrowserUtil
    {
        public static string getDefaultBrowser()
        {
            string browser = string.Empty;
            RegistryKey key = null;
            try
            {
                key = Registry.ClassesRoot.OpenSubKey(@"HTTP\shell\open\command", false);

                //trim off quotes
                browser = key.GetValue(null).ToString().ToLower().Replace("\"", "");
                if (!browser.EndsWith("exe"))
                {
                    //get rid of everything after the ".exe"
                    browser = browser.Substring(0, browser.LastIndexOf(".exe", System.StringComparison.Ordinal) + 4);
                }
            }
            finally
            {
                if (key != null) key.Close();
            }
            return browser;
        }
    }
}
