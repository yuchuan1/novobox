﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Delta.NovoSync.Client
{
    public static class CreateDirectoryExtensions
    {
        public static void CreateDirectory(this DirectoryInfo dirInfo)
        {
            if (dirInfo.Parent != null && !dirInfo.Exists)
                CreateDirectory(dirInfo.Parent);

            if (!dirInfo.Exists)
            {
                dirInfo.Create();
                dirInfo.Refresh();
            }
        }
    }
}
