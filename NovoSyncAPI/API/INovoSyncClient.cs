﻿using System.Collections.Generic;
using Delta.NovoSync.Client.Model;

namespace Delta.NovoSync.Client.API
{
    public interface INovoSyncClient
    {
       // void Initialize(string deviceName, string rootFolderPath);
        void Initialize(string deviceName, string rootFolderPath, bool autoOpenBrowser);
        // Authorization
        string GetRequestToken();
        string Authorize(string deviceName);
        string GetAccessToken();
        string GetAccessToken(string requestToken);
        bool GetIsAuthorized();
        void ResetAuth();
        Auth GetAuth();

        //File operation
        void Sync();
        string GetRootPath();
        IList<MetaData> GetMetaData();
        void CreateFolder(string path);
        void Rename(string path, string newFileNameFullPath);

        // Account Info
        AccountInfo GetAccountInfo();

    }
}