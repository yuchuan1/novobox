﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using RestSharp;

namespace Delta.NovoSync.Client.API
{
    public partial class NovoSyncClient : INovoSyncClient
    {

        private AccountInfo GetAccountInfo(INovoSyncClient novoClient)
        {
            var client = new RestClient
            {
                BaseUrl = ConfigurationManager.AppSettings["NovoSyncBaseUrl"]
                          + ConfigurationManager.AppSettings["AccountInfoUrl"]
            };

            var request = new RestRequest { Method = Method.GET };
            request.AddParameter("oauth_token", novoClient.GetAuth().AccessToken);

            var response = (RestResponse)client.Execute(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new WebException(response.ErrorMessage);

            if (response.ResponseStatus == ResponseStatus.Completed)
                return AccountInfo.Parse(RestResponseHandler(response)); 

            return null;
        }

        public AccountInfo GetAccountInfo()
        {
            return GetAccountInfo(this);
        }
    }
}
