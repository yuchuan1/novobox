﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using Delta.NovoSync.Client.Model;
using RestSharp;

namespace Delta.NovoSync.Client.API
{
    public partial class NovoSyncClient : INovoSyncClient
    {
        // path should begin with a slash '/'

        #region INovoSyncClient Members

        public static IList<MetaData> DownloadFile(INovoSyncClient novoClient, string path)
        {
            var client = new RestClient
                {
                    BaseUrl = ConfigurationManager.AppSettings["NovoSyncBaseUrl"]
                              + ConfigurationManager.AppSettings["DownloadFileUrl"] + path
                };

            client.ClearHandlers();

            var request = new RestRequest {Method = Method.GET};
            request.AddParameter("oauth_token", novoClient.GetAuth().AccessToken);

            try
            {
                _logger.Info("Trying to download file " + path);
                var response = (RestResponse) client.Execute(request);
                if (response.ResponseStatus != ResponseStatus.Completed)
                    throw new WebException(response.ErrorMessage);

                if (response.ResponseStatus == ResponseStatus.Completed)
                {
                    byte[] fileBytes = response.RawBytes;
                    string json = (from item in response.Headers
                                   where item.Name.Equals("x-novosync-header")
                                   select item.Value).Single().ToString();

                    IList<MetaData> metadataList = MetaData.Parse(json, MetaDataType.Download);
                    ByteArrayToFile(novoClient.GetRootPath() + metadataList[0].Path, fileBytes);

                    return metadataList;
                }

                return null;

            }
            catch (Exception ex)
            {
                _logger.Error("Error downloading file " + path + " " + ex.Message);
            }

            return null;
        }

        public static bool ByteArrayToFile(string AbsoluteFilePath, byte[] ByteArray)
        {
            try
            {
                // Open file for reading
                System.IO.FileStream _FileStream = new System.IO.FileStream(AbsoluteFilePath, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                // Writes a block of bytes to this stream using data from a byte array.
                _FileStream.Write(ByteArray, 0, ByteArray.Length);

                // close file stream
                _FileStream.Close();

                return true;
            }
            catch (Exception _Exception)
            {
                // Error
                _logger.Warn("Exception caught in process: {0}", _Exception.ToString());
               
            }

            // error occured, return false
            return false;
        }
        #endregion
    }
}