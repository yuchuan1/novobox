﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using Delta.NovoSync.Client.Model;
using RestSharp;

namespace Delta.NovoSync.Client.API
{
    public partial class NovoSyncClient : INovoSyncClient
    {
        #region INovoSyncClient Members

        public IList<MetaData> GetMetaData()
        {
            return GetMetaData(this);
        }

        public IList<MetaData> GetMetaData(INovoSyncClient novoClient)
        {
            var client = new RestClient
                {
                    BaseUrl = ConfigurationManager.AppSettings["NovoSyncBaseUrl"]
                              + ConfigurationManager.AppSettings["DownloadMetaDataUrl"] + "?oauth_token=" + novoClient.GetAuth().AccessToken
                };

            var request = new RestRequest {Method = Method.GET};
           // request.AddParameter("oauth_token", novoClient.GetAuth().AccessToken);
            var response = (RestResponse) client.Execute(request);

            if (response.StatusCode != HttpStatusCode.OK)
                throw new WebException(response.ErrorMessage + response.StatusDescription);
            
            if(response.ResponseStatus == ResponseStatus.Completed )
                return MetaData.Parse(RestResponseHandler(response), MetaDataType.GetMetadata);

            return null;
        }

        // path should begin with a slash '/'
        public IList<MetaData> GetMetaData(INovoSyncClient novoClient,string path)
        {
            var client = new RestClient
                {
                    BaseUrl = ConfigurationManager.AppSettings["NovoSyncBaseUrl"]
                              + ConfigurationManager.AppSettings["DownloadMetaDataUrl"] + path
                };

            var request = new RestRequest {Method = Method.GET};
            request.AddParameter("oauth_token", _accessToken);
            var response = (RestResponse) client.Execute(request);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new Exception(response.ErrorMessage);

            if (response.ResponseStatus == ResponseStatus.Completed)
                return MetaData.Parse(RestResponseHandler(response), MetaDataType.GetMetadata);

            return null;
        }

        #endregion
    }
}