﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Delta.NovoSync.Client.Model;
using RestSharp;

namespace Delta.NovoSync.Client.API
{
    public partial class NovoSyncClient : INovoSyncClient
    {
        public void CreateFolder(string path)
        {
            CreateFolder(this, path);
        }

        public static IList<MetaData> CreateFolder(INovoSyncClient novoClient, string path)
        {
           // DirectoryInfo di = new DirectoryInfo(path);
           // string serverPath = path.Replace("\\","/").Replace(novoClient.GetRootPath(), string.Empty);

           var client = new RestClient
                {
                    BaseUrl = ConfigurationManager.AppSettings["NovoSyncBaseUrl"]
                              + ConfigurationManager.AppSettings["CreateFolderUrl"]
                };

                var request = new RestRequest { Method = Method.POST };

                request.AddParameter("fullpath", path);
                request.AddParameter("oauth_token", novoClient.GetAuth().AccessToken);


                _logger.Info("Trying to create a folder " + path);
                var response = (RestResponse)client.Execute(request);
                if (response.ResponseStatus != ResponseStatus.Completed)
                    throw new WebException(response.ErrorMessage);

                if (response.ResponseStatus == ResponseStatus.Completed)
                {
                    return MetaData.Parse(RestResponseHandler(response), MetaDataType.Upload);
                }

                return null;
           
        }
    }
}
