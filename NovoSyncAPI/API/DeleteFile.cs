﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using Delta.NovoSync.Client.Model;
using RestSharp;

namespace Delta.NovoSync.Client.API
{
    public partial class NovoSyncClient : INovoSyncClient
    {
        // path should begin with a slash '/'

        #region INovoSyncClient Members

        public static IList<MetaData> DeleteFile(INovoSyncClient novoClient, string path)
        {
            var client = new RestClient
                {
                    BaseUrl = ConfigurationManager.AppSettings["NovoSyncBaseUrl"]
                              + ConfigurationManager.AppSettings["DeleteFileUrl"] + path
                };

            var request = new RestRequest {Method = Method.DELETE};
            request.AddParameter("oauth_token", novoClient.GetAuth().AccessToken);

            _logger.Info("Trying to delete file " + path);
            var response = (RestResponse) client.Execute(request);
            
            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new WebException(response.ErrorMessage);

            if (response.ResponseStatus == ResponseStatus.Completed)
                return MetaData.Parse(RestResponseHandler(response), MetaDataType.Delete);

            return null;
            
        }

        #endregion
    }
}