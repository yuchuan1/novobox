﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using Delta.NovoSync.Client.Model;
using RestSharp;

namespace Delta.NovoSync.Client.API
{
    public partial class NovoSyncClient : INovoSyncClient
    {
        // path should begin with a slash '/'

        #region INovoSyncClient Members

        public static IList<MetaData> RenameFile(INovoSyncClient novoClient, string fromPath, string toPath)
        {
            var client = new RestClient
                {
                    BaseUrl = ConfigurationManager.AppSettings["NovoSyncBaseUrl"]
                              + ConfigurationManager.AppSettings["MoveFileUrl"]
                };

            var request = new RestRequest {Method = Method.POST};
            request.AddParameter("oauth_token", novoClient.GetAuth().AccessToken);
            request.AddParameter("from_path", fromPath);
            request.AddParameter("to_path", toPath);


            var response = (RestResponse) client.Execute(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new WebException(response.ErrorMessage);

            if (response.ResponseStatus == ResponseStatus.Completed)
                return MetaData.Parse(RestResponseHandler(response), MetaDataType.Rename);

            return null;
            
        }

        #endregion

        public void Rename(string path, string newFileNameFullPath)
        {
            RenameFile(this, path, newFileNameFullPath);
        }
    }
}