﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using NLog;
using Newtonsoft.Json;

namespace Delta.NovoSync.Client.API
{
    public class AccountInfo
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public string Email { get; set; }
        public long Quota { get; set; }

        public static AccountInfo Parse(string metadataJson)
        {
            try
            {
                var accountInfo = JsonConvert.DeserializeObject<AccountInfo>(metadataJson);
                return accountInfo;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw;
            }

            return null;
        }
    }
}
