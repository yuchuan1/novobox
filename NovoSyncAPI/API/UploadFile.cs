﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using Delta.NovoSync.Client.Model;
using RestSharp;

namespace Delta.NovoSync.Client.API
{
    public partial class NovoSyncClient : INovoSyncClient
    {
        // path should begin with a slash '/'

        #region INovoSyncClient Members

        public static IList<MetaData> UploadFile(INovoSyncClient novoClient, string path)
        {
            FileInfo fi = new FileInfo(novoClient.GetRootPath() + path);

            string serverPath = path.Replace(novoClient.GetRootPath(), string.Empty);

            if(fi.Exists)
            {
                var client = new RestClient
                                 {
                                     BaseUrl = ConfigurationManager.AppSettings["NovoSyncBaseUrl"]
                                               + ConfigurationManager.AppSettings["UploadFileUrl"] + path + "?oauth_token=" + novoClient.GetAuth().AccessToken
                                 };

                var request = new RestRequest {Method = Method.POST};
                //request.AddFile(fi.Name, novoClient.GetRootPath() + path);
                request.AddFile("file", novoClient.GetRootPath() + path);
               // request.AddParameter("oauth_token", novoClient.GetAuth().AccessToken);

                _logger.Info("Trying to upload file " + path);
                var response = (RestResponse) client.Execute(request);
                if (response.ResponseStatus != ResponseStatus.Completed)
                    throw new WebException(response.ErrorMessage);

                if (response.ResponseStatus == ResponseStatus.Completed)
                {
                    return MetaData.Parse(RestResponseHandler(response), MetaDataType.Upload);
                }

                return null;
                
            }
            else
            {
                throw new FileNotFoundException(serverPath + " not found when uploading!");
            }
        }

        #endregion
    }
}