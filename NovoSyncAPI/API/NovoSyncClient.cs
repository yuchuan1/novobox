﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using Delta.NovoSync.Client.Exceptions;
using Delta.NovoSync.Client.Model;
using NLog;
using RestSharp;

namespace Delta.NovoSync.Client.API
{
    public partial class NovoSyncClient : INovoSyncClient
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public string RootFolderPath = string.Empty;
        public string _accessToken = string.Empty;
        //public IList<MetaData> MetaDataColelction { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NovoSyncClient" /> class.
        /// </summary>
        /// <param name="deviceName">Name of the device.</param>
        /// <param name="rootFolderPath">The root folder path.</param>
        public NovoSyncClient(string deviceName, string rootFolderPath)
        {
            Initialize(deviceName, rootFolderPath);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NovoSyncClient" /> class.
        /// </summary>
        /// <param name="deviceName">Name of the device.</param>
        /// <param name="rootFolderPath">The root folder path.</param>
        /// <param name="autoOpenBrowser">if set to <c>true</c> [auto open browser].</param>
        public NovoSyncClient(string deviceName, string rootFolderPath, bool autoOpenBrowser)
        {
            Initialize(deviceName, rootFolderPath, autoOpenBrowser);
        }

        public bool IsAuthorized { get; set; }
        public Auth OAuth { get; set; }

        #region INovoSyncClient Members

        /// <summary>
        /// Initializes the specified device name.
        /// </summary>
        /// <param name="deviceName">Name of the device.</param>
        /// <param name="rootFolderPath">The root folder path.</param>
        public void Initialize(string deviceName, string rootFolderPath)
        {
            Initialize(deviceName, rootFolderPath, true);
        }

        /// <summary>
        /// Initializes the specified device name.
        /// </summary>
        /// <param name="deviceName">Name of the device.</param>
        /// <param name="rootFolderPath">The root folder path.</param>
        /// <param name="autoOpenBrowser">if set to <c>true</c> [auto open browser].</param>
        /// <exception cref="WebException"></exception>
        public void Initialize(string deviceName, string rootFolderPath, bool autoOpenBrowser)
        {
            IsAuthorized = false;
            try
            {
                if (Auth.GetExistingAuth() == null)
                {
                    if (!GetIsAuthorized())
                    {
                        _logger.Info("No existing OAuth found, requesting for a new one");
                        
                        ResetAuth();

                        _logger.Info("Request Token saved: " + OAuth.RequestToken);
                    }
                }
                else
                {
                    _logger.Info("Retrieving existing request token...");

                    OAuth = Auth.GetExistingAuth();

                    _logger.Info("Request Token found: " + OAuth.RequestToken);
                }
                
                _accessToken = GetAccessToken();

                if (_accessToken != null)
                if (_accessToken.Contains("Access denied"))
                {
                    _logger.Info("Request token is not authorized, redirecting to the web ui for authorization");
                    
                    // Request Token is not valid, resetting auth
                    ResetAuth();

                    if (autoOpenBrowser)
                    {
                        string browser = BrowserUtil.getDefaultBrowser();
                        string url = Authorize(deviceName);

                        var process = new Process
                                          {
                                              StartInfo =
                                                  {
                                                      FileName = browser,
                                                      Arguments = url,
                                                      WindowStyle = ProcessWindowStyle.Normal
                                                  }
                                          };
                        process.Start();
                    }
                }
                else
                {
                    // save access token
                    _logger.Info("Saving access token");
                    if (OAuth.AccessToken == string.Empty)
                    {
                        OAuth.AccessToken = OAuthToken.Parse(_accessToken).oauth_token;
                        OAuth.Save();
                    }

                    try
                    {
                        AccountInfo accountInfo = GetAccountInfo();
                        OAuth.UserEmail = accountInfo.Email;
                        OAuth.Save();
                    }
                    catch (Exception ex)
                    {
                        _logger.Info(ex.Message);
                    }
                   
                    IsAuthorized = true;
                    _logger.Info("Access token saved: " + OAuth.AccessToken);
                }

            }
            catch (WebException ex)
            {
                throw;
                // _logger.Error(ex.Message);
                //throw new NovoSyncServerNotAvailableException(ex.Message);

            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                Auth.Reset();
                throw;
            }

            RootFolderPath = rootFolderPath;

            if (IsAuthorized)
                Sync();
        }

        public void ResetAuth()
        {
            Auth.Reset();
            OAuth = new Auth();
            string requestToken = GetRequestToken();
            OAuth.RequestToken = OAuthToken.Parse(requestToken).oauth_token;
            OAuth.AccessToken = string.Empty;
            OAuth.UserEmail = string.Empty;
            OAuth.Password = string.Empty;
            OAuth.Save();
        }

        public Auth GetAuth()
        {
           return OAuth;
        }

        public bool GetIsAuthorized()
        {
            try
            {
                Auth currentAuth = Auth.GetExistingAuth();

                if (currentAuth == null)
                    return false;

                if (currentAuth.AccessToken != string.Empty && currentAuth.RequestToken != string.Empty)
                    return true;

                //IsAuthorized = !GetAccessToken().Contains("Access denied");
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw;
            }

            return IsAuthorized;
        }

        public void Sync()
        {
            SyncPhysicalFolder();

            IList<MetaData> metadatas = GetMetaData(this);
            //using (var db = new NovoSyncDBEntities())
            //{
            //    _logger.Info("Connection String: " + db.Database.Connection.ConnectionString);
            //    foreach (MetaData metaData in db.MetaDatas)
            //    {
            //        db.MetaDatas.Remove(metaData);
            //    }
            //    db.SaveChanges();
            //}

            // clean up local metadata
            using (var db = new NovoSyncDBEntities())
            {
                foreach (MetaData localMetaData in db.MetaDatas)
                {
                    var path  = localMetaData.Path.Replace("/", "\\");
                    int count = (from item in metadatas
                                 where item.Path.Equals(path)
                                 select item).Count();
                    if (count == 0)
                        db.MetaDatas.Remove(localMetaData);
                }

                db.SaveChanges();
            }

            foreach (MetaData metadata in metadatas)
            {
                // create subfolders
                string synPath = RootFolderPath + metadata.GetFolderPath();
                var di = new DirectoryInfo(synPath);
                di.CreateDirectory();

                if (metadata.Type == "FILE")
                {
                    DownloadFile(this, metadata.Path);
                }
            }
        }
        #endregion

        public void SyncPhysicalFolder()
        {
            IList<MetaData> metadatas = GetMetaData(this);
            var RootDirectoryInfo = new DirectoryInfo(RootFolderPath);

            foreach (string file in Directory.EnumerateFiles(RootFolderPath, "*.*", SearchOption.AllDirectories))
            {
                var fi = new FileInfo(file);
                var path = fi.FullName.Replace(RootDirectoryInfo.FullName, string.Empty).Replace(@"\", @"/");
                int count = (from item in metadatas
                             where item.Type == "FILE"
                                   && item.Path == path.Replace(@"/", @"\")
                             select item).Count();

                if (count == 0 && !path.Contains("desktop.ini"))
                    NovoSyncClient.UploadFile(this, path);
            }

            foreach (string folder in Directory.EnumerateDirectories(RootFolderPath, "*.*", SearchOption.AllDirectories))
            {
                var di = new DirectoryInfo(folder);
                var path = di.FullName.Replace(RootDirectoryInfo.FullName, string.Empty).Replace(@"\", @"/");
                int count = (from item in metadatas
                             where item.Type == "DIRECTORY"
                                   && item.Path == path.Replace(@"/", @"\")
                             select item).Count();

                if (count == 0)
                    NovoSyncClient.CreateFolder(this, path);
            }
        }

        private static string RestResponseHandler(RestResponse response)
        {
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    _logger.Debug("HTTP status code:" + response.StatusCode);
                    _logger.Debug("ResponseUri: " + response.ResponseUri);
                    _logger.Debug("ResponseStatus: " + response.ResponseStatus);
                    _logger.Debug("Content: \n" + response.Content);
                    return response.Content;
                case HttpStatusCode.NotFound:
                    _logger.Error("HTTP status code:" + response.StatusCode);
                    _logger.Error("ResponseUri: " + response.ResponseUri);
                    _logger.Error("ResponseStatus: " + response.ResponseStatus);
                    _logger.Error("Content: \n" + response.Content);
                    return response.Content;
                case HttpStatusCode.Forbidden:
                    _logger.Error("HTTP status code:" + response.StatusCode);
                    _logger.Error("ResponseUri: " + response.ResponseUri);
                    _logger.Error("ResponseStatus: " + response.ResponseStatus);
                    _logger.Error("Content: \n" + response.Content);
                    return response.Content;
                case HttpStatusCode.InternalServerError:
                    _logger.Error("HTTP status code:" + response.StatusCode);
                    _logger.Error("ResponseUri: " + response.ResponseUri);
                    _logger.Error("ResponseStatus: " + response.ResponseStatus);
                    _logger.Error("Content: \n" + response.Content);
                    return response.Content;
                case HttpStatusCode.BadGateway:
                    _logger.Error("HTTP status code:" + response.StatusCode);
                    _logger.Error("ResponseUri: " + response.ResponseUri);
                    _logger.Error("ResponseStatus: " + response.ResponseStatus);
                    _logger.Error("Content: \n" + response.Content);
                    return response.Content;
                case HttpStatusCode.BadRequest:
                    _logger.Error("HTTP status code:" + response.StatusCode);
                    _logger.Error("ResponseUri: " + response.ResponseUri);
                    _logger.Error("ResponseStatus: " + response.ResponseStatus);
                    _logger.Error("Content: \n" + response.Content);
                    return response.Content;
                default:
                    _logger.Error("HTTP status code:" + response.StatusCode);
                    _logger.Error("ResponseUri: " + response.ResponseUri);
                    _logger.Error("ResponseStatus: " + response.ResponseStatus);
                    _logger.Error("Content: \n" + response.Content);
                    return response.Content;
            }
        }
    }
}