﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.NetworkInformation;
using RestSharp;

namespace Delta.NovoSync.Client.API
{
    public partial class NovoSyncClient : INovoSyncClient
    {
        #region INovoSyncClient Members

        public string GetRequestToken()
        {
            var client = new RestClient
                {
                    BaseUrl = ConfigurationManager.AppSettings["NovoSyncBaseUrl"]
                              + ConfigurationManager.AppSettings["RequestTokenUrl"]
                };

            var request = new RestRequest {Method = Method.GET};

            var response = (RestResponse) client.Execute(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new WebException(response.ErrorMessage);

            if (response.ResponseStatus == ResponseStatus.Completed)
                return RestResponseHandler(response);

            return null;
        }

        public string Authorize(string deviceName)
        {
            string BaseUrl = ConfigurationManager.AppSettings["NovoSyncBaseUrl"]
                             + ConfigurationManager.AppSettings["AuthorizeUrl"] + "?oauth_token=" + OAuth.RequestToken + "&" + "devicename=" + deviceName;
            return BaseUrl;
        }

        public string GetAccessToken()
        {
            return GetAccessToken(OAuth.RequestToken);
        }

        public string GetAccessToken(string requestToken)
        {
            var client = new RestClient
            {
                BaseUrl = ConfigurationManager.AppSettings["NovoSyncBaseUrl"]
                          + ConfigurationManager.AppSettings["AccessTokenUrl"]
            };

            var request = new RestRequest { Method = Method.GET };
            request.AddParameter("oauth_token", requestToken);

            var response = (RestResponse)client.Execute(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new WebException(response.ErrorMessage);

            if (response.ResponseStatus == ResponseStatus.Completed)
                return RestResponseHandler(response);

            return null;
        }

        public string GetRootPath()
        {
            return RootFolderPath;
        }
        
        #endregion
    }
}