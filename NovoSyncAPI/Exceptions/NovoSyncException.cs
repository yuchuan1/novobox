﻿using System;
using System.Net;
using RestSharp;

namespace Delta.NovoSync.Client.Exceptions
{
    public class NovoSyncException : Exception
    {
        public NovoSyncException()
        {
        }

        public NovoSyncException(string message)
            : base(message)
        {
        }

        public NovoSyncException(IRestResponse r)
        {
            Response = r;
            StatusCode = r.StatusCode;
        }

        public HttpStatusCode StatusCode { get; set; }

        /// <summary>
        /// The response of the error call (for Debugging use)
        /// </summary>
        public IRestResponse Response { get; private set; }
    }

}