﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using RestSharp;

namespace Delta.NovoSync.Client.Exceptions
{
    public class NovoSyncServerNotAvailableException : NovoSyncException
    {
        public NovoSyncServerNotAvailableException()
        {
        }

        public NovoSyncServerNotAvailableException(string message) : base(message)
        {
        }

        public NovoSyncServerNotAvailableException(IRestResponse r) : base(r)
        {
            Response = r;
            StatusCode = r.StatusCode;
        }


        public HttpStatusCode StatusCode { get; set; }

        /// <summary>
        /// The response of the error call (for Debugging use)
        /// </summary>
        public IRestResponse Response { get; private set; }
    }
}
