﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace NovoSyncServer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class SystemConfig : Window
    {
        private FolderBrowserDialog folderBrowserDialog;

        public SystemConfig()
        {
           InitializeComponent();
           folderBrowserDialog = new FolderBrowserDialog();
           folderBrowserDialog.Description = "Select the directory that you want to use as the default.";
        }

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            DialogResult result = folderBrowserDialog.ShowDialog();
            txtRepositoryPath.Text = folderBrowserDialog.SelectedPath;
        }
    }
}
