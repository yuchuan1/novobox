﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.Win32;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using Selenium;
using Automation = System.Windows.Automation;
using System.Windows.Automation;

namespace Delta.NovoBox.Tests
{
    [TestFixture]
    public class UITests
    {
        private string _programLocation;
        public static IWebDriver WebDriver;

        [TestFixtureSetUp]
        public void Init()
        {
            killProcess("chrome");
            killProcess("NovoBox");
           // WebDriver = new ChromeDriver();

            _programLocation = @"D:\Delta.Net\novobox\NovoBox\bin\Debug\NovoBox.exe";
            var process = new Process
            {
                StartInfo =
                {
                    FileName = _programLocation

                }
            };
            process.Start();

            // wait for program to start running
            Thread.Sleep(10000);
        }

        [TestFixtureTearDown]
        public void Dispose()
        {
            try
            {
                WebDriver.Quit();
            }
            catch (Exception)
            {
                
            }
            
        }

        [Test]
        public void TestLink()
        {
            AutomationElement rootElement = AutomationElement.RootElement;
            if (rootElement != null)
            {
                // Condition for Chrome
                PropertyCondition nameChromeCondition = new PropertyCondition(AutomationElement.NameProperty, "Google Chrome");
                PropertyCondition classNameChromeCondition = new PropertyCondition(AutomationElement.ClassNameProperty, "Chrome_OmniboxView");
                AndCondition condition = new AndCondition(nameChromeCondition, classNameChromeCondition);

                // Condition for NovoBox
                PropertyCondition namePaneCondition = new PropertyCondition(AutomationElement.NameProperty, "NovoBox Preferences");
                PropertyCondition typePaneCondition = new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Window);
                AndCondition novoBoxCondition = new AndCondition(namePaneCondition, typePaneCondition);

                //Condition for Link button
                PropertyCondition typeLinkCondition = new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Button);
                PropertyCondition nameLinkCondition = new PropertyCondition(AutomationElement.NameProperty, "Link");
                AndCondition linkCondition = new AndCondition(typeLinkCondition, nameLinkCondition);

                //Condition for Unlink button
                PropertyCondition typeUnlinkCondition = new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Button);
                PropertyCondition nameUnlinkCondition = new PropertyCondition(AutomationElement.NameProperty, "Unlink");
                AndCondition unlinkCondition = new AndCondition(typeUnlinkCondition, nameUnlinkCondition);

                var appElement = rootElement.FindFirst(TreeScope.Subtree, classNameChromeCondition);
                if (appElement != null)
                {
                    ValuePattern pattern = appElement.GetCurrentPattern(ValuePattern.Pattern) as ValuePattern;

                    Uri url = new Uri(pattern.Current.Value);

                    WebDriver = new ChromeDriver();
                    if(pattern.Current.Value.Contains(@"/login"))
                    {
                        WebDriver.Navigate().GoToUrl(url);
                        WebDriver.FindElement(By.Id("login_email")).Clear();
                        WebDriver.FindElement(By.Id("login_email")).SendKeys("test@test.com");
                        WebDriver.FindElement(By.Name("login_password")).Clear();
                        WebDriver.FindElement(By.Name("login_password")).SendKeys("ab1234");
                        WebDriver.FindElement(By.Id("signin")).Click();
                        Thread.Sleep(5000);

                        url = new Uri(WebDriver.Url);
                        WebDriver.Quit();
                    }
                   
                    //press login button, click yes
                    
                    killProcess("chrome");

                    appElement = rootElement.FindFirst(TreeScope.Subtree, novoBoxCondition);
                    if(appElement!=null)
                    {
                        AutomationElement element = appElement.FindFirst(TreeScope.Subtree, linkCondition);
                        if(element != null)
                        {
                            InvokePattern iPattern = element.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                            if(iPattern != null)
                                iPattern.Invoke();
                            else
                                Assert.Fail("Cannot find link button");
                        }
                        else
                        {
                            element = appElement.FindFirst(TreeScope.Subtree, unlinkCondition);
                            if(element !=null)
                            {
                                InvokePattern iPattern = element.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                                if (iPattern != null)
                                    iPattern.Invoke();
                            }
                            else
                            {
                                Assert.Fail("Cannot find link or unlink button");
                            }
                        }
                    }
                    else
                    {
                        Assert.Fail("Cannot find NovoBox Preferences window");
                    }
                }
                else
                {
                    Assert.Fail("Cannot open Google Chrome!");
                }
            }
            
        }

        [Test]
        public void TestUnlink()
        {
            OpenNovoSyncPreferenceUI();

            AutomationElement rootElement = AutomationElement.RootElement;
            if (rootElement != null)
            {
                PropertyCondition namePaneCondition = new PropertyCondition(AutomationElement.NameProperty, "NovoBox Preferences");
                PropertyCondition typePaneCondition = new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Window);
                AndCondition condition = new AndCondition(namePaneCondition, typePaneCondition);

                //Condition for Unlink button
                PropertyCondition typeLinkCondition = new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Button);
                PropertyCondition nameLinkCondition = new PropertyCondition(AutomationElement.NameProperty, "Unlink");
                AndCondition unlinkCondition = new AndCondition(typeLinkCondition, nameLinkCondition);

                var novoBox = rootElement.FindFirst(TreeScope.Subtree, condition);
                if(novoBox != null)
                {
                    AutomationElement element = novoBox.FindFirst(TreeScope.Subtree, unlinkCondition);
                    if (element != null)
                    {
                        InvokePattern iPattern = element.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                        if (iPattern != null)
                            iPattern.Invoke();
                        else
                            Assert.Fail("Error clicking unlink button");
                    }
                    else
                    {
                        Assert.Fail("Cannot find unlink button");
                    }
                }
                else
                {
                    Assert.Fail("Cannot find Link button");
                }

            }

        }

        [Test]
        public void TestStartNovoBoxOnStartCheckBox()
        {
            string RUN_LOCATION = @"Software\Microsoft\Windows\CurrentVersion\Run";

            OpenNovoSyncPreferenceUI();

            AutomationElement rootElement = AutomationElement.RootElement;
            if (rootElement != null)
            {
                PropertyCondition namePaneCondition = new PropertyCondition(AutomationElement.NameProperty, "NovoBox Preferences");
                PropertyCondition typePaneCondition = new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Window);
                AndCondition condition = new AndCondition(namePaneCondition, typePaneCondition);

                var appElement = rootElement.FindFirst(TreeScope.Subtree, condition);
                AutomationElement chkBox = GetCheckBoxElement(appElement, "chkAutoStart");
                var temp = chkBox.GetCurrentPattern(TogglePattern.Pattern) as TogglePattern;
                if (temp != null)
                    if (temp.Current.ToggleState == ToggleState.Off)
                    {
                        temp.Toggle();
                        var openSubKey = Registry.CurrentUser.OpenSubKey(RUN_LOCATION);
                        if (openSubKey != null)
                        {
                            Assert.True(openSubKey.GetValueNames().Contains("NovoBox"));
                        }
                        else Assert.Fail("Registry key " + RUN_LOCATION + " not found!");

                        temp.Toggle();

                        openSubKey = Registry.CurrentUser.OpenSubKey(RUN_LOCATION);
                        if (openSubKey != null)
                        {
                            Assert.False(openSubKey.GetValueNames().Contains("NovoBox"));
                        }
                        else Assert.Fail("Registry key " + RUN_LOCATION + " not found!");
                    }
            }
        }

        private void OpenNovoSyncPreferenceUI()
        {
            AutomationElement rootElement = AutomationElement.RootElement;

            object oPattern;
            if (rootElement != null)
            {
                PropertyCondition namePaneCondition = new PropertyCondition(AutomationElement.NameProperty, "");
                PropertyCondition typePaneCondition = new PropertyCondition(AutomationElement.ControlTypeProperty,
                                                                            ControlType.Pane);
                PropertyCondition classPaneCondition = new PropertyCondition(AutomationElement.ClassNameProperty,
                                                                             "Shell_TrayWnd");

                PropertyCondition nameDesktopPaneCondition = new PropertyCondition(AutomationElement.NameProperty,
                                                                                   "Desktop");
                PropertyCondition typeDesktopPaneCondition = new PropertyCondition(
                    AutomationElement.ControlTypeProperty, ControlType.Pane);
                PropertyCondition classDesktopPaneCondition = new PropertyCondition(
                    AutomationElement.ClassNameProperty, "Desktop");

                PropertyCondition nameButtonCondition = new PropertyCondition(AutomationElement.NameProperty,
                                                                              "NotificationChevron");
                PropertyCondition typeButtonCondition = new PropertyCondition(AutomationElement.ControlTypeProperty,
                                                                              ControlType.Button);

                PropertyCondition typeNotificationPaneCondition =
                    new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Pane);
                PropertyCondition classNotificationPaneCondition =
                    new PropertyCondition(AutomationElement.ClassNameProperty, "TrayNotifyWnd");

                PropertyCondition nameNotificationOverflow = new PropertyCondition(AutomationElement.ClassNameProperty,
                                                                                   "NotifyIconOverflowWindow");

                PropertyCondition nameOverFlowArea = new PropertyCondition(AutomationElement.NameProperty,
                                                                           "Overflow Notification Area");
                PropertyCondition nameNovoBox = new PropertyCondition(AutomationElement.NameProperty, "NovoBox");


                AndCondition notificationPaneCondition = new AndCondition(typeNotificationPaneCondition,
                                                                          classNotificationPaneCondition);
                AndCondition notificationButtonCondition = new AndCondition(nameButtonCondition, typeButtonCondition);

                AndCondition condition = new AndCondition(namePaneCondition, typePaneCondition);
                condition = new AndCondition(condition, classPaneCondition);

                AutomationElement appElement = rootElement.FindFirst(TreeScope.Children, condition);

                if (appElement != null)
                {
                    //AutomationElement trayButton = appElement.FindFirst(TreeScope.Children, nameButtonCondition);
                    var notificationPane = appElement.FindFirst(TreeScope.Children, notificationPaneCondition);
                    if (notificationPane != null)
                    {
                        AutomationElement notificationElement = notificationPane.FindFirst(TreeScope.Children,
                                                                                           notificationButtonCondition);

                        InvokePattern pattern =
                            notificationElement.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;

                        var panes = notificationElement.FindAll(TreeScope.Children, typePaneCondition);

                        condition = new AndCondition(typePaneCondition, nameNotificationOverflow);
                        pattern.Invoke();
                        var appElement1 = rootElement.FindFirst(TreeScope.Children, condition);
                        if (appElement1 != null)
                        {
                            var toolBar = appElement1.FindFirst(TreeScope.Children, nameOverFlowArea);
                            if (toolBar != null)
                            {
                                var novoBox = toolBar.FindFirst(TreeScope.Children, nameNovoBox);
                                if (novoBox != null)
                                {
                                    InvokePattern pattern1 =
                                        novoBox.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                                    pattern1.Invoke();
                                    pattern1.Invoke();
                                }

                            }

                        }



                    }
                }
            }
        }

        private AutomationElement GetCheckBoxElement(AutomationElement parentElement, string value)
        {
            Automation.Condition condition = new PropertyCondition(AutomationElement.AutomationIdProperty, value);
            AutomationElement chkElement = parentElement.FindFirst(TreeScope.Descendants, condition);
            return chkElement;
        }

        private AutomationElement GetTextElement(AutomationElement parentElement, string value)
        {
            Automation.Condition condition = new PropertyCondition(AutomationElement.AutomationIdProperty, value);
            AutomationElement txtElement = parentElement.FindFirst(TreeScope.Descendants, condition);
            return txtElement;
        }

        private void killProcess(string processName)
        {
            Process[] localByName = Process.GetProcessesByName(processName);
            foreach (Process p in localByName)
            {
                p.Kill();
            }
        }

        private void ClickYes()
        {
            string baseURL = "http://localhost:9000/login";

            Uri url = new Uri(baseURL);
            WebDriver = new ChromeDriver();
            WebDriver.Navigate().GoToUrl(url);
            WebDriver.FindElement(By.Id("login_email")).Clear();
            WebDriver.FindElement(By.Id("login_email")).SendKeys("test@test.com");
            WebDriver.FindElement(By.Name("login_password")).Clear();
            WebDriver.FindElement(By.Name("login_password")).SendKeys("ab1234");
            WebDriver.FindElement(By.Id("signin")).Click();
            WebDriver.FindElement(By.CssSelector("button.button")).Click();
            
        }

       
    }
}
